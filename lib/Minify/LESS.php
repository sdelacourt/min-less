<?php
/**
 * Class LessCss_Minify  
 * @package LessCss
 */

require_once 'lib/Minify/CSS.php';
/**
 * Minify CSS
 *
 * This class uses Minify_CSS and lessphp to compress LESS sources
 * 
 * @package LessCss
 * @author Marco Pivetta <ocramius@gmail.com>
 * @author http://marco-pivetta.com/
 */
class Minify_LESS extends Minify_CSS {
    
    private $_less = '';
    private $_options = array();
    
    public function __construct($less, $options = array())
    {
        $this->_less = $less;
        $this->_options = $options;
    }

    
    /**
     * Minify a LESS string
     * 
     * @param string $less
     * 
     * @param array $options available options:
     * 
     * @inheritdoc
     * 
     * @return string
     */
    public static function minify($less, $options = array()) 
    {
        $min = new self($less, $options);
        return $min->process();
    }
    
    public function process()
    {
        require_once 'lib/lessphp/lessc.inc.php';
        
        $lessc = new lessc();
        $ret = $lessc->compileFile($this->_options['dir'] . '/' . $this->_options['id']);
        return (parent::minify($ret, $this->_options));
    }
}